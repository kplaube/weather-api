# Journal

I am keeping track of the decisions taken during the development of the assignment.

## Git history

This journal is being updated with code changes and git commits. Traveling back in time (using Git) might show different ideas than the one being presented right now.

## Technology

### The language

In "normal conditions", Python would be my first option (with some micro-framework like Flask) to develop the solution. But considering the technology at Shape, I think it's more "honest" to deliver a Java solution. With that in mind, not just the architectural and design decisions can be
evaluated, but the code itself.

I was considering using Micronaut as web framework, because of its "micro" nature. But after [looking into some comparisons](https://medium.com/better-programming/which-java-microservice-framework-should-you-choose-in-2020-4e306a478e58), Spring (Boot) with its default configuration still offers good numbers, and it seems to be easier to get it up and running (also, it's the framework I'm using nowadays).

### Blocking x Non-blocking

Looking at the assignment, and how the weather service works, I believe the Spring-MVC/Servet option is enough. It might demand some async processing, which can make the code move towards a non-blocking
approach.

### SDK

I found a Java SDK to could be used as part of the solution, but the code seems a bit outdated. Since it's about calling a small number of endpoints (probably just one), I'll go with Spring's RestTemplate.

## The business logic

### What to fetch from OpenWeather

I'm going to assume that `locationId` mentioned in the assignment can be translated to `cityId` on OpenWeather.

Following the requirements described in the assignment, we need to know the forecast for the following days:

- Next day (I assume it's tomorrow if I'm using the app today)
- Next five days

The call to https://openweathermap.org/forecast5 endpoint should be enough to fulfill the logic.

#### Bulk downloading

One option would be bulk downloading. With a scheduled job, we could fetch forecast for ~210k cities at once (CSV file) and store it, so we don't have to hit OpenWeather for a certain amount of time.

The subscription plan that allows us to do that also enables us to do 30k calls/min, well higher than the constraint set in the assignment. So, keeping in mind the context of the task, I'm not considering the scheduled job option.

### The assignment solution

The idea is to "granularly" cache the requests to `forecast5`; this is going to change our upper bound from requests to the number of locations. 10k locations per day is not a "foolproof solution," but at least our boundaries are well known now, and a need to scale will be easier to predict.

In time, it's a matter of perspective. 10k locations cover only ~4% of the OWA dataset, and we can do better by using the bulk downloading, suggested above. Still, 10k locations are more than enough to cover Brazil ([which has 5.570 counties](https://pt.wikipedia.org/wiki/Lista_de_estados_brasileiros_por_n%C3%BAmero_de_munic%C3%ADpios)).

Also, we'll be converting the temperature units by ourselves, instead of making different requests to the same location, with different temperatures.

## API contract

I'll assume we already have clients consuming OWA, so it would be nice to keep the same payload, in order to respect the previously agreed contract.

But for the `/summary` endpoint, we need to add a custom business logic, and therefore, a new API contract.

The draft is the following:

```json
[{
    "id": 524901,
    "name": "Moscow",
    "dt": 1587837600,
    "dt_txt": "2020-04-25 18:00:00",
    "temperatures": [{
        "current": 275.69,
        "minimum": 276.36,
        "maximum": 276.69
    }]
}]
```

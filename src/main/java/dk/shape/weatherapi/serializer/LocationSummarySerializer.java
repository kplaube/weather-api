package dk.shape.weatherapi.serializer;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import dk.shape.weatherapi.model.City;
import dk.shape.weatherapi.model.Forecast;
import dk.shape.weatherapi.model.Location;
import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.Temperature;
import dk.shape.weatherapi.model.TemperatureSummary;

// TODO: Missing unit tests
public class LocationSummarySerializer {

    public static LocationSummary of(Location location, LocalDate date) {
        final City city = location.getCity();
        final List<Forecast> forecastsForDate = getForecastsForDate(location.getForecasts(), date);

        return LocationSummary.builder()
                .cityId(city.getId())
                .cityName(city.getName())
                .temperatures(
                        forecastsForDate.stream().map(forecast -> {
                            final Temperature temperature = forecast.getTemperature();
                            return TemperatureSummary.builder()
                                    .datetime(forecast.getDatetime())
                                    .current(temperature.getCurrent())
                                    .maximum(temperature.getMaximum())
                                    .minimum(temperature.getMinimum())
                                    .build();
                        }).collect(Collectors.toList())
                ).build();
    }

    private static List<Forecast> getForecastsForDate(List<Forecast> forecasts, LocalDate date) {
        return forecasts.stream()
                .filter(forecast -> forecast.getLocalDateObject().equals(date))
                .collect(Collectors.toList());
    }
}

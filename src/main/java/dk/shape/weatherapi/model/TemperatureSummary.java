package dk.shape.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TemperatureSummary {
    private Double current;
    private Double maximum;
    private Double minimum;
    @JsonProperty("dt_txt")
    private String datetime;
}

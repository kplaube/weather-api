package dk.shape.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Wind {
    private Double speed;

    @JsonProperty("deg")
    private Integer directionDegree;
}

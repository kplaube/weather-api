package dk.shape.weatherapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Location {
    private City city;

    @JsonProperty("cod")
    private String code;

    @JsonProperty("list")
    private List<Forecast> forecasts;

    private Integer message;

    @JsonProperty("cnt")
    private Integer numberOfLines;
}

package dk.shape.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Temperature {
    @JsonProperty("temp")
    private Double current;

    @JsonProperty("feels_like")
    private Double feelsLike;

    @JsonProperty("grnd_level")
    private Integer groundLevel;

    private Integer humidity;

    @JsonProperty("temp_max")
    private Double maximum;

    @JsonProperty("temp_min")
    private Double minimum;

    private Integer pressure;

    @JsonProperty("sea_level")
    private Integer seaLevel;

    @JsonProperty("temp_kf")
    private Double tempKf;


}

package dk.shape.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Weather {
    private Integer id;

    @JsonProperty("main")
    private String value;

    private String description;

    private String icon;
}

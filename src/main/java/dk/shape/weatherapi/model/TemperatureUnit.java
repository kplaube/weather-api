package dk.shape.weatherapi.model;

import java.util.function.Function;

public enum TemperatureUnit {
    celsius("metric", s -> s - 273.15),
    fahrenheit("imperial", s -> ((s - 273.15) * 1.8) + 32),
    kelvin("default", s -> s);

    private final String unit;
    private final Function<Double, Double> fn;

    TemperatureUnit(String unit, Function<Double, Double> fn) {
        this.unit = unit;
        this.fn = fn;
    }

    public Double convertFrom(Double temperatureInKelvin) {
        return this.fn.apply(temperatureInKelvin);
    }
}

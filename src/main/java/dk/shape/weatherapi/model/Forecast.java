package dk.shape.weatherapi.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Forecast {
    private Map<String, Integer> clouds;

    @JsonProperty("dt_txt")
    private String datetime;

    @JsonProperty("main")
    private Temperature temperature;

    @JsonProperty("dt")
    private Integer timestamp;

    private List<Weather> weather;

    private Wind wind;

    @JsonIgnore
    public LocalDate getLocalDateObject() {
        return LocalDate.ofInstant(new Date(timestamp * 1000L).toInstant(), ZoneId.systemDefault());
    }
}

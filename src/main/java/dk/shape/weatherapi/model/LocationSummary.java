package dk.shape.weatherapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LocationSummary {
    @JsonProperty("id")
    private Integer cityId;

    @JsonProperty("name")
    private String cityName;

    @JsonProperty("temperatures")
    private List<TemperatureSummary> temperatures;
}

package dk.shape.weatherapi.model;

import java.util.Map;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class City {
    private Integer id;
    private String name;
    private Map<String, Double> coord;
    private String country;
    private Integer sunrise;
    private Integer sunset;
    private Integer timezone;
}

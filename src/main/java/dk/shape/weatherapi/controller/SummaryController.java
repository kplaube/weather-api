package dk.shape.weatherapi.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.TemperatureUnit;
import dk.shape.weatherapi.service.FavoriteLocationsService;

@RestController
@RequestMapping("/summary")
public class SummaryController {

    private FavoriteLocationsService service;

    public SummaryController(FavoriteLocationsService service) {
        this.service = service;
    }

    @GetMapping
    public List<LocationSummary> getSummary(@RequestParam String unit, @RequestParam String locations,
                                            @RequestParam Double temperature) {
        final List<Integer> listOfLocations = formatLocations(locations);
        final TemperatureUnit temperatureUnit = TemperatureUnit.valueOf(unit);

        return service.filterByTemperature(listOfLocations, temperature, temperatureUnit);
    }

    private List<Integer> formatLocations(String locations) {
        return Stream.of(locations.split(",")).map(Integer::valueOf).collect(Collectors.toList());
    }
}

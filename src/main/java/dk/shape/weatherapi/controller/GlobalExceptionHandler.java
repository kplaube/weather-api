package dk.shape.weatherapi.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HttpClientErrorException.class)
    protected ResponseEntity<Object> handleHttpClientErrorException(RuntimeException ex, WebRequest request) {
        final HttpClientErrorException exception = (HttpClientErrorException) ex;
        final String body = exception.getResponseBodyAsString();
        final HttpStatus status = exception.getStatusCode();
        return getEntity(ex, request, body, status);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleBadRequest(RuntimeException ex, WebRequest request) {
        return getEntity(ex, request, null, HttpStatus.BAD_REQUEST);

    }

    private ResponseEntity<Object> getEntity(RuntimeException ex, WebRequest request, String body, HttpStatus status) {
        return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
    }
}

package dk.shape.weatherapi.controller;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import dk.shape.weatherapi.model.Location;
import dk.shape.weatherapi.service.ForecastService;

@RestController
@RequestMapping("/locations")
public class LocationController {

    private ForecastService forecastService;

    public LocationController(ForecastService forecastService) {
        this.forecastService = forecastService;
    }

    @GetMapping("/{locationId}")
    public Location getLocation(@PathVariable Integer locationId) {
        final Optional<Location> location = forecastService.getByLocationId(locationId);

        if (location.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Location not found");
        }

        return location.get();
    }
}

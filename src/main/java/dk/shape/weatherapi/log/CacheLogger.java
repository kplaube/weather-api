package dk.shape.weatherapi.log;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CacheLogger implements CacheEventListener<Object, Object> {

    @Override
    public void onEvent(CacheEvent<?, ?> cacheEvent) {
        log.info("Cache event: {}, {}", cacheEvent.getType(), cacheEvent.getKey());
    }
}

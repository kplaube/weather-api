package dk.shape.weatherapi.service;

import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.TemperatureUnit;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class OpenWeatherFavoriteLocationsService implements FavoriteLocationsService {

    private Clock clock;
    private ForecastService forecastService;

    @Autowired
    public OpenWeatherFavoriteLocationsService(ForecastService forecastService, Clock clock) {
        this.clock = clock;
        this.forecastService = forecastService;
    }

    @Override
    public List<LocationSummary> filterByTemperature(List<Integer> locationIds, Double temperature,
                                                     TemperatureUnit unit) {
        final LocalDate tomorrow = LocalDate.now(clock).plusDays(1);

        log.info("Filtering locations: {}, by forecast date: {}, and temperature: {}", locationIds, tomorrow,
                temperature);

        return getLocationSummaryStream(locationIds, tomorrow).filter(location ->
                location.getTemperatures().stream()
                        .anyMatch(hourlyForecast -> temperature < unit.convertFrom(hourlyForecast.getMaximum()))
        ).collect(Collectors.toList());
    }

    private Stream<LocationSummary> getLocationSummaryStream(List<Integer> locationIds, LocalDate date) {
        return locationIds.stream()
                .map(s -> forecastService.getLocationSummaryById(s, date))
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
}

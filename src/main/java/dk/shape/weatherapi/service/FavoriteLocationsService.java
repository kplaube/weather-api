package dk.shape.weatherapi.service;

import java.util.List;

import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.TemperatureUnit;

public interface FavoriteLocationsService {
    List<LocationSummary> filterByTemperature(List<Integer> locationIds, Double temperature, TemperatureUnit unit);
}

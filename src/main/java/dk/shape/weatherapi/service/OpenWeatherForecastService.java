package dk.shape.weatherapi.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import dk.shape.weatherapi.model.Location;
import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.serializer.LocationSummarySerializer;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class OpenWeatherForecastService implements ForecastService {
    private String apiAddress;
    private String appId;
    private RestTemplate restTemplate;

    @Autowired
    public OpenWeatherForecastService(RestTemplate restTemplate, @Value("${weather-api.address}") String apiAddress,
                                      @Value("${weather-api.app-id}") String appId) {
        this.restTemplate = restTemplate;
        this.apiAddress = apiAddress;
        this.appId = appId;
    }

    @Override
    @Cacheable(value = "locations", key = "#id")
    public Optional<Location> getByLocationId(Integer id) {
        final String endpoint = String.format("%s/forecast?id=%s&appid=%s", apiAddress,
                id, appId);
        log.info("Fetching location from: {}", endpoint);

        return Optional.ofNullable(restTemplate.getForObject(endpoint, Location.class));
    }

    // TODO: Missing unit tests
    public Optional<LocationSummary> getLocationSummaryById(Integer id, LocalDate date) {
        return getByLocationId(id).map(location -> LocationSummarySerializer.of(location, date));
    }
}

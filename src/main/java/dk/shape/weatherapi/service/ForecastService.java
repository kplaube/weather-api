package dk.shape.weatherapi.service;

import java.time.LocalDate;
import java.util.Optional;

import dk.shape.weatherapi.model.Location;
import dk.shape.weatherapi.model.LocationSummary;

public interface ForecastService {
    Optional<Location> getByLocationId(Integer id);
    Optional<LocationSummary> getLocationSummaryById(Integer id, LocalDate date);
}

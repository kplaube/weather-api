package dk.shape.weatherapi.builder;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Map;

import dk.shape.weatherapi.model.City;
import dk.shape.weatherapi.model.Forecast;
import dk.shape.weatherapi.model.Location;
import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.Temperature;
import dk.shape.weatherapi.model.Weather;
import dk.shape.weatherapi.model.Wind;
import dk.shape.weatherapi.serializer.LocationSummarySerializer;

public class LocationFixture {
    public static final Integer MOSCOW_ID = 524901;
    public static final Integer SCHROEDER_ID = 3447929;
    public static final LocalDate DATE = LocalDate.of(2020, Month.APRIL, 25);

    public static LocationSummary summarizedMoscow() {
        return LocationSummarySerializer.of(moscow(), DATE);
    }

    public static LocationSummary summarizedSchroeder() {
        return LocationSummarySerializer.of(schroeder(), DATE);
    }

    public static Location moscow() {
        var city = City.builder()
                .id(MOSCOW_ID)
                .name("Moscow")
                .coord(Map.of("lat", 55.7522, "lon", 37.6156))
                .country("RU")
                .sunrise(1587779969)
                .sunset(1587833707)
                .timezone(10800)
                .build();

        var forecasts = List.of(
                Forecast.builder()
                        .clouds(Map.of("all", 64))
                        .datetime("2020-04-25 18:00:00")
                        .temperature(
                                Temperature.builder()
                                        .current(276.64)
                                        .feelsLike(273.55)
                                        .groundLevel(981)
                                        .humidity(74)
                                        .maximum(276.64)
                                        .minimum(276.36)
                                        .pressure(998)
                                        .seaLevel(998)
                                        .tempKf(0.28)
                                        .build()
                        )
                        .timestamp(1587837600)
                        .weather(List.of(
                                Weather.builder()
                                        .id(803)
                                        .value("Clouds")
                                        .description("broken clouds")
                                        .icon("04n")
                                        .build()
                        ))
                        .wind(Wind.builder().speed(1.44).directionDegree(357).build())
                        .build(),
                Forecast.builder()
                        .datetime("2020-04-26 03:00:00")
                        .temperature(Temperature
                                .builder().maximum(274.98).build())
                        .timestamp(1587870000)
                        .build()
        );

        return Location.builder()
                .city(city)
                .code("200")
                .forecasts(forecasts)
                .message(0)
                .numberOfLines(forecasts.size())
                .build();
    }

    public static Location schroeder() {
        var city = City.builder()
                .id(SCHROEDER_ID)
                .name("Schroeder")
                .build();

        var forecasts = List.of(
                Forecast.builder()
                        .datetime("2020-04-25 21:00:00")
                        .temperature(Temperature.builder()
                                .maximum(277.0)
                                .build())
                        .timestamp(1587848400)
                        .build(),
                Forecast.builder()
                        .datetime("2020-04-26 03:00:00")
                        .temperature(Temperature.builder()
                                .maximum(278.98)
                                .build())
                        .timestamp(1587848400)
                        .build()
        );

        return Location.builder()
                .city(city)
                .code("200")
                .forecasts(forecasts)
                .message(0)
                .numberOfLines(forecasts.size())
                .build();
    }
}

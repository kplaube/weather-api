package dk.shape.weatherapi.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;

import dk.shape.weatherapi.builder.LocationFixture;
import dk.shape.weatherapi.model.Location;
import dk.shape.weatherapi.service.ForecastService;

@WebMvcTest(LocationController.class)
public class LocationControllerTest {
    private static final Integer LOCATION_ID = 524901;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ForecastService forecastService;

    private Location location;

    @BeforeEach
    void setUp() {
        location = LocationFixture.moscow();
    }

    @Test
    void whenLocationIdExists_thenReturnSuccess() throws Exception {
        mockGetByLocationId();

        mockMvc.perform(get("/locations/" + LOCATION_ID))
                .andExpect(status().isOk());
    }

    @Test
    void whenLocationIdExists_thenReturnPayloadWithCity() throws Exception {
        mockGetByLocationId();

        var city = location.getCity();

        mockMvc.perform(get("/locations/" + LOCATION_ID))
                .andExpect(jsonPath("$.city.id", is(city.getId())))
                .andExpect(jsonPath("$.city.name", is(city.getName())))
                .andExpect(jsonPath("$.city.coord", is(city.getCoord())))
                .andExpect(jsonPath("$.city.country", is(city.getCountry())))
                .andExpect(jsonPath("$.city.sunrise", is(city.getSunrise())))
                .andExpect(jsonPath("$.city.sunset", is(city.getSunset())))
                .andExpect(jsonPath("$.city.timezone", is(city.getTimezone())));
    }

    @Test
    void whenLocationIdExists_thenReturnPayloadWithListOfTemperatures() throws Exception {
        mockGetByLocationId();

        var forecast = location.getForecasts().get(0);
        var temperature = forecast.getTemperature();
        var weather = forecast.getWeather().get(0);
        var wind = forecast.getWind();

        mockMvc.perform(get("/locations/" + LOCATION_ID))
                .andExpect(jsonPath("$.list[0].clouds", is(forecast.getClouds())))
                .andExpect(jsonPath("$.list[0].dt", is(forecast.getTimestamp())))
                .andExpect(jsonPath("$.list[0].dt_txt", is(forecast.getDatetime())))
                .andExpect(jsonPath("$.list[0].main.feels_like", is(temperature.getFeelsLike())))
                .andExpect(jsonPath("$.list[0].main.humidity", is(temperature.getHumidity())))
                .andExpect(jsonPath("$.list[0].main.pressure", is(temperature.getPressure())))
                .andExpect(jsonPath("$.list[0].main.sea_level", is(temperature.getSeaLevel())))
                .andExpect(jsonPath("$.list[0].main.temp_kf", is(temperature.getTempKf())))
                .andExpect(jsonPath("$.list[0].main.temp", is(temperature.getCurrent())))
                .andExpect(jsonPath("$.list[0].main.grnd_level", is(temperature.getGroundLevel())))
                .andExpect(jsonPath("$.list[0].main.temp_max", is(temperature.getMaximum())))
                .andExpect(jsonPath("$.list[0].main.temp_min", is(temperature.getMinimum())))
                .andExpect(jsonPath("$.list[0].weather[0].id", is(weather.getId())))
                .andExpect(jsonPath("$.list[0].weather[0].description", is(weather.getDescription())))
                .andExpect(jsonPath("$.list[0].weather[0].icon", is(weather.getIcon())))
                .andExpect(jsonPath("$.list[0].weather[0].main", is(weather.getValue())))
                .andExpect(jsonPath("$.list[0].wind.speed", is(wind.getSpeed())))
                .andExpect(jsonPath("$.list[0].wind.deg", is(wind.getDirectionDegree())));
    }

    @Test
    void whenLocationIdIsEmpty_thenReturnNotFound() throws Exception {
        when(forecastService.getByLocationId(LOCATION_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get("/locations/" + LOCATION_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenLocationIsDoesNotExist_thenReturnNotFound() throws Exception {
        doThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND)).when(forecastService).getByLocationId(LOCATION_ID);

        mockMvc.perform(get("/locations/" + LOCATION_ID))
                .andExpect(status().isNotFound());
    }

    private void mockGetByLocationId() {
        when(forecastService.getByLocationId(LOCATION_ID)).thenReturn(Optional.of(location));
    }
}

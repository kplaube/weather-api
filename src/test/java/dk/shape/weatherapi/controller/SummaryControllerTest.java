package dk.shape.weatherapi.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import dk.shape.weatherapi.builder.LocationFixture;
import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.TemperatureUnit;
import dk.shape.weatherapi.service.FavoriteLocationsService;

@WebMvcTest(SummaryController.class)
public class SummaryControllerTest {
    private static final List<Integer> LOCATIONS = List.of(524901, 3447929);
    private static final Double TEMPERATURE = 30.0;
    private static final TemperatureUnit TEMPERATURE_UNIT = TemperatureUnit.celsius;
    private static final String VALID_URL = "/summary?temperature=30&unit=celsius&locations=524901,3447929";
    private static final LocationSummary MOSCOW = LocationFixture.summarizedMoscow();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FavoriteLocationsService favoriteLocationsService;

    @Test
    void whenRequestIsValid_thenReturnSuccess() throws Exception {
        mockFilterByTemperature();

        mockMvc.perform(get(VALID_URL))
                .andExpect(status().isOk());
    }

    @Test
    void whenRequestIsValid_thenReturnAListOfCities() throws Exception {
        mockFilterByTemperature();
        var temperature = MOSCOW.getTemperatures().get(0);

        mockMvc.perform(get(VALID_URL))
                .andExpect(jsonPath("$[0].id", is(MOSCOW.getCityId())))
                .andExpect(jsonPath("$[0].name", is(MOSCOW.getCityName())))
                .andExpect(jsonPath("$[0].temperatures[0].current", is(temperature.getCurrent())))
                .andExpect(jsonPath("$[0].temperatures[0].dt_txt", is(temperature.getDatetime())))
                .andExpect(jsonPath("$[0].temperatures[0].maximum", is(temperature.getMaximum())))
                .andExpect(jsonPath("$[0].temperatures[0].minimum", is(temperature.getMinimum())));
    }

    @Test
    void whenRequestIsMissingTemperature_thenReturnBadRequest() throws Exception {
        mockMvc.perform(get("/summary?unit=celsius&locations=524901,3447929"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenRequestIsMissingLocations_thenReturnBadRequest() throws Exception {
        mockMvc.perform(get("/summary?temperature=30&unit=celsius"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenRequestHasInvalidDataTypeForLocations_thenReturnBadRequest() throws Exception {
        mockMvc.perform(get("/summary?temperature=30&unit=celsius&locations=524901,a"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenRequestIsMissingUnit_thenReturnBadRequest() throws Exception {
        mockMvc.perform(get("/summary?temperature=30&locations=524901,3447929"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenRequestHasAnInvalidUnit_thenReturnBadRequest() throws Exception {
        mockMvc.perform(get("/summary?temperature=30&unit=invalid&locations=524901,3447929"))
                .andExpect(status().isBadRequest());
    }

    private void mockFilterByTemperature() {
        when(favoriteLocationsService.filterByTemperature(LOCATIONS, TEMPERATURE, TEMPERATURE_UNIT)).thenReturn(
                List.of(MOSCOW)
        );
    }
}

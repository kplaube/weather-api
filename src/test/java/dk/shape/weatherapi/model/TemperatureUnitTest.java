package dk.shape.weatherapi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TemperatureUnitTest {

   @Test
    void convertFrom_returnsKelvinToCelsius() {
        assertEquals(37.0, TemperatureUnit.celsius.convertFrom(310.15));
    }


   @Test
    void convertFrom_returnsKelvinToFarenheit() {
        assertEquals(67.73000000000005, TemperatureUnit.fahrenheit.convertFrom(293.0));
    }

   @Test
    void convertFrom_doesnotChangeKelvinValue() {
        assertEquals(270.0, TemperatureUnit.kelvin.convertFrom(270.0));
    }

}

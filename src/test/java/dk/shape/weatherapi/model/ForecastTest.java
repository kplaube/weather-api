package dk.shape.weatherapi.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.Month;

import org.junit.jupiter.api.Test;

public class ForecastTest {

    @Test
    void getLocalDateObject_shouldReturnValidDateObject() {
        var forecast = Forecast.builder().timestamp(1587837600).build();
        var expected = LocalDate.of(2020, Month.APRIL, 25);

        assertTrue(expected.equals(forecast.getLocalDateObject()));
    }
}

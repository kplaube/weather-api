package dk.shape.weatherapi.service;

import static dk.shape.weatherapi.builder.LocationFixture.MOSCOW_ID;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import dk.shape.weatherapi.builder.LocationFixture;
import dk.shape.weatherapi.model.Location;

@ExtendWith(MockitoExtension.class)
public class OpenWeatherForecastServiceTest {
    private static final String API_ADDRESS = "https://api.openweathermap.org/data/2.5";
    private static final String APP_ID = "should-be-an-app-id";
    private static final String ENDPOINT = String.format("%s/forecast?id=%s&appid=%s", API_ADDRESS, MOSCOW_ID,
            APP_ID);

    @Mock
    private RestTemplate restTemplate;

    private ForecastService service;

    private Location location;

    @BeforeEach
    void setUp() {
        location = LocationFixture.moscow();
        service = new OpenWeatherForecastService(restTemplate, API_ADDRESS, APP_ID);
    }

    @Test
    void getByLocationId_returnsForecastForLocation() {
        when(restTemplate.getForObject(ENDPOINT, Location.class)).thenReturn(location);

        var result = service.getByLocationId(MOSCOW_ID);

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals(location, result.get());
    }

    @Test
    void getByLocationId_returnsEmptyIfTheResponseIsNull() {
        when(restTemplate.getForObject(ENDPOINT, Location.class)).thenReturn(null);

        var result = service.getByLocationId(MOSCOW_ID);

        Assertions.assertFalse(result.isPresent());
    }
}

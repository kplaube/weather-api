package dk.shape.weatherapi.service;

import static dk.shape.weatherapi.builder.LocationFixture.MOSCOW_ID;
import static dk.shape.weatherapi.builder.LocationFixture.SCHROEDER_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import dk.shape.weatherapi.builder.LocationFixture;
import dk.shape.weatherapi.model.LocationSummary;
import dk.shape.weatherapi.model.TemperatureUnit;

@ExtendWith(MockitoExtension.class)
public class OpenWeatherFavoriteLocationsServiceTest {
    private static final Double TEMPERATURE_IN_CELSIUS = 1.85;
    private static final Double TEMPERATURE_IN_FARENHEIT = 35.33;
    private static final Double TEMPERATURE_IN_KELVIN = 275.0;
    private static final TemperatureUnit CELSIUS = TemperatureUnit.celsius;
    private static final TemperatureUnit FARENHEIT = TemperatureUnit.fahrenheit;
    private static final TemperatureUnit KELVIN = TemperatureUnit.kelvin;
    private static final LocalDateTime TODAY = LocalDateTime.of(2020, Month.APRIL, 24, 18, 0);

    @Mock
    private ForecastService forecastService;

    private FavoriteLocationsService service;

    @BeforeEach
    void setUp() {
        final Clock clock = Clock.fixed(
                TODAY.toInstant(ZoneOffset.UTC),
                ZoneId.systemDefault());
        service = new OpenWeatherFavoriteLocationsService(forecastService, clock);
    }

    @Test
    void filterByTemperature_returnsLocationWhenThereIsHigherTemperatureThanExpected() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());
        mockGetLocationSummaryById(SCHROEDER_ID, LocationFixture.summarizedSchroeder());

        var result = service.filterByTemperature(List.of(MOSCOW_ID, SCHROEDER_ID), TEMPERATURE_IN_KELVIN, KELVIN);

        assertEquals(2, result.size());
        assertEquals("Moscow", result.get(0).getCityName());
        assertEquals("Schroeder", result.get(1).getCityName());
    }

    @Test
    void filterByTemperature_returnsLocationWhenThereIsHigherTemperatureThanExpected_inCelsus() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());
        mockGetLocationSummaryById(SCHROEDER_ID, LocationFixture.summarizedSchroeder());

        var result = service.filterByTemperature(List.of(MOSCOW_ID, SCHROEDER_ID), TEMPERATURE_IN_CELSIUS, CELSIUS);

        assertEquals(2, result.size());
    }

    @Test
    void filterByTemperature_returnsLocationWhenThereIsHigherTemperatureThanExpected_inFarenheit() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());
        mockGetLocationSummaryById(SCHROEDER_ID, LocationFixture.summarizedSchroeder());

        var result = service.filterByTemperature(List.of(MOSCOW_ID, SCHROEDER_ID), TEMPERATURE_IN_FARENHEIT, FARENHEIT);

        assertEquals(2, result.size());
    }

    @Test
    void filterByTemperature_returnsOnlyLocationsWithExpectedTemperature() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());
        mockGetLocationSummaryById(SCHROEDER_ID, LocationFixture.summarizedSchroeder());

        var higherThanMoscowLesserThanSchroeder = 276.65;
        var result = service.filterByTemperature(List.of(MOSCOW_ID, SCHROEDER_ID),
                higherThanMoscowLesserThanSchroeder, KELVIN);

        assertEquals(1, result.size());
        assertEquals("Schroeder", result.get(0).getCityName());
    }

    @Test
    void filterByTemperature_returnsOnlyLocationsWithExpectedTemperature_inCelsius() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());
        mockGetLocationSummaryById(SCHROEDER_ID, LocationFixture.summarizedSchroeder());

        var higherThanMoscowLesserThanSchroeder = 3.5;
        var result = service.filterByTemperature(List.of(MOSCOW_ID, SCHROEDER_ID),
                higherThanMoscowLesserThanSchroeder, CELSIUS);

        assertEquals(1, result.size());
        assertEquals("Schroeder", result.get(0).getCityName());
    }

    @Test
    void filterByTemperature_returnsOnlyLocationsWithExpectedTemperature_inFarenheit() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());
        mockGetLocationSummaryById(SCHROEDER_ID, LocationFixture.summarizedSchroeder());

        var higherThanMoscowLesserThanSchroeder = 38.3;
        var result = service.filterByTemperature(List.of(MOSCOW_ID, SCHROEDER_ID),
                higherThanMoscowLesserThanSchroeder, FARENHEIT);

        assertEquals(1, result.size());
        assertEquals("Schroeder", result.get(0).getCityName());
    }

    @Test
    void filterByTemperature_returnsEmptyListWhenThereIsNoMaximumTemperatureHigherThanExpected() {
        mockGetLocationSummaryById(MOSCOW_ID, LocationFixture.summarizedMoscow());

        var aboveMaximumTemperatureNextDay = 277.0;
        var result = service.filterByTemperature(List.of(MOSCOW_ID), aboveMaximumTemperatureNextDay,
                KELVIN);

        assertEquals(Collections.emptyList(), result);
    }

    @Test
    void filterByTemperature_returnsEmptyListWhenLocationsIsEmpty() {
        var result = service.filterByTemperature(Collections.emptyList(), TEMPERATURE_IN_KELVIN,
                KELVIN);

        assertEquals(Collections.emptyList(), result);
    }

    @Test
    void filterByTemperature_returnsEmptyListWhenLocationHasEmptyTemperatures() {
        var location = LocationSummary.builder().temperatures(Collections.emptyList()).build();
        mockGetLocationSummaryById(MOSCOW_ID, location);

        var result = service.filterByTemperature(List.of(MOSCOW_ID), TEMPERATURE_IN_KELVIN, KELVIN);

        assertEquals(Collections.emptyList(), result);
    }

    private void mockGetLocationSummaryById(Integer locationId, LocationSummary locationSummary) {
        when(forecastService.getLocationSummaryById(locationId, TODAY.plusDays(1).toLocalDate())).thenReturn(Optional.of(locationSummary));
    }
}

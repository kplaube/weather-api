# Weather API

To be aware of the rationale behind the decisions, please read the `JOURNAL.md` file.

## Installing

It's required Maven and Java 11. Once you have it, you can use the `install` task:

```shell script
mvn install
```

## Running

The project uses Spring Boot + Maven Plugin:

```shell script
mvn spring-boot:run
```

## Tests

The tests can be run via `tests` task:

```shell script
mvn test
```